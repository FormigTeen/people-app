import React, { useEffect } from 'react';
import { Text, View, Image } from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';
import styled from 'styled-components'

const Detail = () => {
    const { params } = useRoute()
    const { goBack } = useNavigation()

    useEffect(() => {
        if ( !params.person ) {
            goBack()
        }
    }, [])

    return (
        <Container>
            <ImagePeople source={{
                uri: params.person.picture.large
            }} />
            <Table>
                <TableRow label="Email">{params.person.email}</TableRow>
                <TableRow label="Cidade">{params.person.location.city}</TableRow>
                <TableRow label="Estado">{params.person.location.state}</TableRow>
                <TableRow label="Telefone">{params.person.phone}</TableRow>
                <TableRow label="Celular">{params.person.cell}</TableRow>
                <TableRow label="Nacionalidade">{params.person.nat}</TableRow>
            </Table>

        </Container>
    )
}

const TableRow = ({ label, children }) => (
    <Row>
        <Cell bold>{label}:</Cell>
        <Cell>{children}</Cell>
    </Row>

)

const ImagePeople = styled(Image)`
    aspect-ratio: 1;
`

const Container = styled(View)`
    padding: 15px;
`

const Table = styled(View)`
    background-color: #E2F9FF;
    margin-top: 20px;
    elevation: 1
`

const Row = styled(View)`
    flex-direction: row;
    padding: 3px;
    border-width: 1px;
`

const Cell = styled(Text)`
    font-size: 16px;
    padding: 5px;
    flex: 4;
    ${props => props.bold &&`
        flex: 2; 
        font-weight: bold;
    `}
`

export default Detail
