import React, { useState, useEffect } from 'react';
import { Text, View } from 'react-native';
import List from '../components/List';

const Home = () => (
    <View>
        <List />
    </View>
)

export default Home
