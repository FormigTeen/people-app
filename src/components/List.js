import useAxios from 'axios-hooks'
import { Text, View, Image, TouchableOpacity } from 'react-native';
import React, { useState, useEffect } from 'react';
import styled from 'styled-components'
import { useNavigation } from '@react-navigation/native';

const List = () => {
    const [{data , loading}] = useAxios("https://randomuser.me/api/?nat=br&results=5")
    const [results, setResults] = useState(null)
    const { push } = useNavigation()

    useEffect(() => {
        setResults(loading ? null : data.results);
    }, [loading])

    return (
        <Choose>
            <When condition={results == null}>
                <Text>Testando...</Text>
            </When>
            <Otherwise>
                <For each='person' index='idx' of={results}>
                    <Container key={idx}>
                        <People onPress={() => push('Detail', { person })} person={person} />
                    </Container>
                </For>
            </Otherwise>
        </Choose>
    )
}

const People = ({ person: { name, picture }, onPress }) => (
    <TouchableOpacity onPress={onPress}>
        <ItemBox>
            <Avatar source={{ uri: picture.thumbnail }} />
            <TextBox>{name.first} {name.last}</TextBox>
        </ItemBox>
    </TouchableOpacity>
)

const Container = styled(View)`
    background-color: #E2F9FF;
`

const ItemBox = styled(View)`
    height: 60px;
    border-bottom-width: 1px;
    border-bottom-color: #BBB;
    align-items: center;
    flexDirection: row;
`

const TextBox = styled(Text)`
    font-size: 20px;
    padding-left: 15px;
    flex: 7;
`

const Avatar = styled(Image)`
    aspect-ratio: 1;
    flex: 1;
    margin-left: 15px;
    border-radius: 50px;
`

export default List
