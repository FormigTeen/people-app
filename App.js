import React, { useState, useEffect } from 'react';
import Home from './src/scenes/Home';
import Detail from './src/scenes/Detail';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator()

const App = () => (
    <NavigationContainer>
        <Stack.Navigator 
            initialRouteName='Home'
            screenOptions={{
                title: 'Inicio',
                headerTintColor: '#C5C5C5',
                headerTitleAlign: 'center',
                headerStyle: {
                    backgroundColor: '#D35400',
                    borderBottomColor: '#C5C5C5',
                },
                headerTitleStyle: {
                    fontSize: 30,
                    color: '#FFF',
                }
            }}
        >
            <Stack.Screen name='Inicio' component={Home} />
            <Stack.Screen name='Detail' 
                options={({ route }) => ({
                    title: route.params.person.name.first
                })}
                component={Detail} />
        </Stack.Navigator>
    </NavigationContainer>
)

export default App

